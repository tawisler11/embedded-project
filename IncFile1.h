/*
 * IncFile1.h
 *
 * Created: 10/31/2013 11:31:52 PM
 *  Author: rdhammond11
 */ 

#ifndef INCFILE1_H_
#define INCFILE1_H_
#define QSIZE 10

typedef enum{AM,PM} clockMessages;
typedef struct{int8_t hr; int8_t minute; int8_t noMessage; clockMessages AM_or_PM; } clock_time;
typedef struct{clock_time Buffer[QSIZE]; clock_time* front; clock_time* back;} Queue_clocktime_types;

extern Queue_clocktime_types time_Storage;


#endif /* INCFILE1_H_ */