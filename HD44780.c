//  HD44780 LCD 4-bit IO mode Driver
//  (C) 2009 - 2012 Radu Motisan , radu.motisan@gmail.com , www.pocketmagic.net
//  All rights reserved.
//
//  HD44780.c: Definitions for LCD command instructions
//  The constants define the various LCD controller instructions which can be passed to the 
//  function lcd_command(), see HD44780 data sheet for a complete description.

#include <avr/io.h> 
#include "HD44780.h"
#include "aux_globals.h"

//-----------------------------------------------------------------------------------------
// FUNCTION: lcd_toggle_e
// PURPOSE: flush channel E
void lcd_toggle_e(void)
{
    lcd_e_high();
    delay_us(10);
    lcd_e_low();
}

//-----------------------------------------------------------------------------------------
// FUNCTION: lcd_clrscr
// PURPOSE:  Clear display and set cursor to home position
void lcd_clrscr(void)
{
	//g_nCurrentLine = 0;
	lcd_instr(1<<LCD_CLR);
	delay_us(500);
}

//-----------------------------------------------------------------------------------------
// FUNCTION: lcd_home
// PURPOSE:  Set cursor to home position
void lcd_home(void)
{
	//g_nCurrentLine = 0;
	lcd_instr(1<<LCD_HOME);
	delay_us(500);
}

//-----------------------------------------------------------------------------------------
// FUNCTION: lcd_waitbusy
// PURPOSE: loops while lcd is busy, returns address counter
static uint8_t lcd_waitbusy(void)
{
    /* the address counter is updated 4us after the busy flag is cleared */
    delay_us(250);//250   
}/* lcd_waitbusy */

//-----------------------------------------------------------------------------------------
// FUNCTION: lcd_write
// PURPOSE: send a character or an instruction to the LCD
void lcd_write(uint8_t data,uint8_t rs) 
{
	lcd_waitbusy();
	
	//check write type
    if (rs){   
       lcd_rs_high(); //write data
	}	   
    else{     
       lcd_rs_low();  //write instruciton
	}	   
    
    // clear PORTA
	
	PORTA = 0;
	
	if(data & 0x80) PORTA |= _BV(LCD_DATA7_PIN);
	if(data & 0x40) PORTA |= _BV(LCD_DATA6_PIN);
	if(data & 0x20) PORTA |= _BV(LCD_DATA5_PIN);
	if(data & 0x10) PORTA |= _BV(LCD_DATA4_PIN);    
	if(data & 0x08) PORTA |= _BV(LCD_DATA3_PIN);
	if(data & 0x04) PORTA |= _BV(LCD_DATA2_PIN);
	if(data & 0x02) PORTA |= _BV(LCD_DATA1_PIN);
	if(data & 0x01) PORTA |= _BV(LCD_DATA0_PIN);
    lcd_toggle_e();        
    
    // all data pins high (inactive) 
    PORTA |= _BV(LCD_DATA0_PIN);
    PORTA |= _BV(LCD_DATA1_PIN);
    PORTA |= _BV(LCD_DATA2_PIN);
    PORTA |= _BV(LCD_DATA3_PIN);
	PORTA |= _BV(LCD_DATA4_PIN);
	PORTA |= _BV(LCD_DATA5_PIN);
	PORTA |= _BV(LCD_DATA6_PIN);
	PORTA |= _BV(LCD_DATA7_PIN);
}

//-----------------------------------------------------------------------------------------

void lcd_string(char *text)
{
	char c;
	while ( (c = *text++) )  lcd_char(c);
}

// FUNCTION: lcd_char
// PURPOSE:  send a character to the LCD
void lcd_char(uint8_t data)
{
	if (data=='\n') 
	{
		if (g_nCurrentLine >= LCD_LINES - 1)
			lcd_setline(0);
		else
			lcd_setline(g_nCurrentLine+1);
	}
	else
	lcd_write(data,1);
}

//-----------------------------------------------------------------------------------------
// FUNCTION: lcd_instr
// PURPOSE:  send an instruction to the LCD
void lcd_instr(uint8_t instr)
{
	lcd_write(instr,0);
}

//-----------------------------------------------------------------------------------------
// FUNCTION: lcd_init
// PURPOSE:  Initialize LCD
void lcd_init(void)
{
    // wait 25ms or more after power-on
    delay_us(25000);
	
	PORTA = 0;
	PORTB = 0;
	
    // initial write to lcd is 8bit 
    PORTA |= _BV(LCD_DATA5_PIN);  // _BV(LCD_FUNCTION)>>4;
    PORTA |= _BV(LCD_DATA4_PIN);  // _BV(LCD_FUNCTION_8BIT)>>4;
    lcd_toggle_e();
    delay_us(5000); //2000        // delay, busy flag can't be checked here 

    
	// repeat last command 
    lcd_toggle_e();      
    delay_us(2000); //64          // delay, busy flag can't be checked here 
	
	lcd_toggle_e();      
    delay_us(2000);
	
	PORTB |= _BV(2);

    lcd_instr(LCD_FUNCTION_8BIT_2LINES);
	delay_us(1000);       
	
	lcd_instr(LCD_DISP_OFF);
	delay_us(1000);          
	
	lcd_clrscr();	
	delay_us(1000);  		
	
	lcd_instr(LCD_ENTRY_INC_);
	delay_us(1000);          	
	
	lcd_instr(LCD_DISP_ON);
	delay_us(1000);
	
	lcd_instr(LCD_MOVE_CURSOR_RIGHT);
	delay_us(1000);
	
    lcd_home();
    delay_us(1000);
}

//-----------------------------------------------------------------------------------------
// FUNCTION: lcd_newline
// PURPOSE:  Move cursor on specified line


int g_nCurrentLine = 0;

//-----------------------------------------------------------------------------------------
// FUNCTION: lcd_string
// PURPOSE:  send a null terminated string to the LCD eg. char x[10]="hello!";


void lcd_setline(uint8_t line)
{
    uint8_t addressCounter = 0;
	switch(line)
	{
		case 0: addressCounter = LCD_START_LINE1; break;
		case 1: addressCounter = LCD_START_LINE2; break;
		case 2: addressCounter = LCD_START_LINE3; break;
		case 3: addressCounter = LCD_START_LINE4; break;
		default:addressCounter = LCD_START_LINE1; break;
	}
	g_nCurrentLine = line;

    lcd_instr((1<<LCD_DDRAM)+addressCounter);
}

void lcd_string2(char *szFormat, ...)
{	
    char szBuffer[256]; //in this buffer we form the message
    int NUMCHARS = sizeof(szBuffer) / sizeof(szBuffer[0]);
    int LASTCHAR = NUMCHARS - 1;
    va_list pArgs;
    va_start(pArgs, szFormat);
    vsnprintf(szBuffer, NUMCHARS - 1, szFormat, pArgs);
    va_end(pArgs);
	
	lcd_string(szBuffer);

}
//-----------------------------------------------------------------------------------------
// FUNCTION: lcd_gotoxy
// PURPOSE:  Set cursor to specified position
//           Input:    x  horizontal position  (0: left most position)
//                     y  vertical position    (0: first line)
void lcd_gotoxy(uint8_t x, uint8_t y)
{
#if LCD_LINES==1
    lcd_instr((1<<LCD_DDRAM)+LCD_START_LINE1+x);
#elif LCD_LINES==2
   	switch (y) 
	{
		case 0:lcd_instr((1<<LCD_DDRAM)+LCD_START_LINE1+x);break;
    	case 1:lcd_instr((1<<LCD_DDRAM)+LCD_START_LINE2+x);break;
		default: break;
	}
#elif LCD_LINES==4
   	switch (y) 
	{
		case 0:lcd_instr((1<<LCD_DDRAM)+LCD_START_LINE1+x);break;
    	case 1:lcd_instr((1<<LCD_DDRAM)+LCD_START_LINE2+x);break;
    	case 2:lcd_instr((1<<LCD_DDRAM)+LCD_START_LINE3+x);break;
    	case 3:lcd_instr((1<<LCD_DDRAM)+LCD_START_LINE4+x);break;
		default: break;
	}
#endif
}


