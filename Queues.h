/*
 * IncFile1.h
 *
 * Created: 10/31/2013 11:31:52 PM
 *  Author: rdhammond11
 */ 

#ifndef INCFILE1_H_
#define INCFILE1_H_
#define QSIZE 10

typedef enum{AM,PM} clockMessages;

/* SET MODE ENUM .... All the possible modes the button can send as a message */
typedef enum {NORMAL, ALARM_SET} setMode_msg_t;

/* INCREMENTOR .... Messages sent to increment or decrement time */
typedef enum {HR_INC, MIN_INC, NO_MESSAGE} hrMinSet_msg_t;

/* ALARM SET ... messages sent by the alarm */
typedef enum {ON, OFF} alarm_msg_t;

/* TICK!!!!!!! */
typedef enum {tick} tick_msg_t;
	
typedef enum{ONSTATE, OFFSTATE} alarmONOFFState;

/////////////////////---------STRUCTURES------------ ////////////////////////////
/* Structure to hold all of the button messages for the queue */
typedef struct{setMode_msg_t modeSelect; hrMinSet_msg_t hrMin_Inc; alarm_msg_t alarmState;} buttonInput;
/*                            Structure for clock times for the queue                                 */
typedef struct{int8_t hr; int8_t minute; int8_t noMessage; clockMessages AM_or_PM; } clock_time;
/*                            Structure for alarm enable/ disable                                    */	
typedef struct{alarm_msg_t alarmState;} alarm_input;
									/* Ticks struct */
typedef struct{tick_msg_t tick_inc;} tickInput;
/*                              Queue for the clock time types                                       */	
typedef struct{clock_time Buffer[QSIZE]; clock_time* front; clock_time* back;} Queue_clocktime_types;
/*                              Queue for the button input                                           */
typedef struct{buttonInput Buffer[QSIZE]; buttonInput* front; buttonInput* back;} Queue_buttonInput_types;
/*                              Queue for the tick input                                           */
typedef struct{tickInput Buffer[QSIZE]; tickInput* front; tickInput* back;} Queue_tickInput_types;
/*                              Queue for alarm enable/disable                                      */
typedef struct{alarm_input Buffer[QSIZE]; alarm_input* front; alarm_input* back;} Queue_alarmInput_types;

extern Queue_clocktime_types time_Storage;
extern Queue_buttonInput_types input_button_Queue;
extern Queue_tickInput_types tick_Storage;
extern Queue_clocktime_types alarm_Storage;
extern Queue_tickInput_types alarm_state_Tick_Storage;



#endif /* INCFILE1_H_ */