/*
 * DisplayTask.c
 *
 * Created: 11/3/2013 1:27:58 PM
 *  Author: tawisler11
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdbool.h>
#include "Queues.h"
#include "HD44780.h"

extern Queue_clocktime_types time_Storage;
extern Queue_clocktime_types alarm_Storage;

clock_time dequeue(Queue_clocktime_types *q);
bool isQueueEmpty(const Queue_clocktime_types *q);

static void updateDisplay(clock_time temp);

void displayTask(void)
{
	
	
	if(!isQueueEmpty(&alarm_Storage))
	{
		clock_time tempAlarm = dequeue(&alarm_Storage);
		updateDisplay(tempAlarm);
		lcd_string(" ALRM");
	}
	
	if(!isQueueEmpty(&time_Storage))
	{
		clock_time temp = dequeue(&time_Storage);
		updateDisplay(temp);
	}
	
}

static void updateDisplay(clock_time temp)
{	
	int bigHour, hour, bigMin, min;
	
	bigHour = temp.hr/10 + (int)'0';
	hour = temp.hr%10 + (int)'0';
	bigMin = temp.minute/10 + (int)'0';
	min = temp.minute%10 + (int)'0';
	
	/*
	char tempAMPM[];
	
	if(temp.AM_or_PM == AM)
	{
		strcpy(tempAMPM, "AM");
	}else{
		strcpy(tempAMPM, "PM");
	}
	*/
	
	lcd_clrscr();
	lcd_home();
	
	
	if(temp.hr/10 == 0)
	{
		lcd_string(" ");	
	}		
	else
	{
		lcd_char(bigHour);
	}		
	lcd_char(hour);
	lcd_string(":");
	lcd_char(bigMin);
	lcd_char(min);
	lcd_string(" ");
	//lcd_string(tempAMPM);
	if(temp.AM_or_PM == AM)
	{
		lcd_string("AM");
	}
	else
	{
		lcd_string("PM");
	}
}