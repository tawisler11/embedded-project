/*
 * LCDTest.c
 *
 * Created: 10/28/2013 2:02:00 PM
 *  Author: tawisler11
 */ 


#include <avr/io.h>
#include "aux_globals.h"
#include "HD44780.h"

int main(void)
{
	DDRD = 0xFF;	//Port used for Data
	DDRB = 0xFF;	//Port used for EN and RS (RW is grounded)
	
	lcd_init();								// init the LCD screen
	
	char helloString[] = "hello world";
	char TaylorString[] = "Taylor";
	

	lcd_string(helloString);
	lcd_string(" 154637413674621462426 ");
	lcd_string(TaylorString);

	while(1)
	{
		;
	}
	
}