/*
 * GccApplication1.c
 *
 * Created: 10/17/2013 4:01:46 PM
 *  Author: rdhammond11
 */ 



/*
 * fsm2013.c
 *
 * PURPOSE: DEMONSTRATE A STATE MACHINE USING POINTERS, ARRAYS,
 * STRUCTS, AND TYPEDEF.
 *
 * Created: 10/3/2013 10:27:17 AM
 *  Author: carrolls, Function lookup_transition by: Richard Hammond 
 */ 

#include <avr/io.h>
#include "Queues.h"
/*Modified from QRDL's State Machine from Stack Overflow*/

/**************************************************************************************************
*									Global Vars
***************************************************************************************************
*/


/*
 **************************************************************************************************
 *									Global Function Declarations
 **************************************************************************************************
 */

void enqueueButton(buttonInput msg, Queue_buttonInput_types *q);
extern Queue_buttonInput_types input_button_Queue;
extern Queue_tickInput_types alarm_Tick_Storage;
buttonInput buttonInputVar;

/*
 **************************************************************************************************
 *									Definitions and Declarations
 **************************************************************************************************
 */

#define NUM_TRANS 16
#define EXIT_STATE End
#define ENTRY_STATE Entry

typedef enum {PASSIVE, BH1, BH2, BH3, ACTIVE, BL1, BL2, BL3} state_codes_t; //B0 means button was pressed and B1 means button is low (Active low)

/* SECOND -- There is an "input alphabet"*/
typedef enum {LOW, HIGH, JUST_PRESSED, JUST_RELEASED} msg_t; /* Input values */


	
typedef struct  {
	state_codes_t src_state;
	msg_t   ret_code;
	state_codes_t dst_state;
}transition_t;

state_codes_t qInc = PASSIVE;
state_codes_t qDec = PASSIVE;
state_codes_t qClear = PASSIVE;
state_codes_t qSetAlarm = PASSIVE;

extern int16_t pulse;
extern clock_time clockVar;
extern Queue_clocktime_types time_Storage;

/*
 **************************************************************************************************
 *									Static Function Declarations
 **************************************************************************************************
 */

static msg_t SW0_action(void);
static msg_t SW1_action(void);
static msg_t SW2_action(void);
static msg_t SW3_action(void);
static msg_t SW3_offAction(void);
static msg_t SW0_nothing_action(void);
static msg_t SW1_nothing_action(void);
static msg_t SW2_nothing_action(void);
static msg_t SW3_nothing_action(void);
static state_codes_t fsm(state_codes_t q, transition_t STT[],msg_t (*moore_table[])(void));
static state_codes_t lookup_transition(state_codes_t q0, msg_t m, transition_t STT[]);


/*
 **************************************************************************************************
 *									Function Pointer Arrays
 **************************************************************************************************
 */

msg_t (*buttonIncrement_FncPtr[])(void) = {  
	
	[BL3] =		SW0_action, 
	[ACTIVE] =  SW0_nothing_action, 
	[BL1] =		SW0_nothing_action, 
	[BL2] =		SW0_nothing_action,
	[PASSIVE] = SW0_nothing_action,
	[BH1] =		SW0_nothing_action,
	[BH2] =		SW0_nothing_action,
	[BH3] =		SW0_nothing_action  
	
};
			
msg_t (*buttonDecrement_FncPtr[])(void)= {
	
	[BL3] =		SW1_action,
	[ACTIVE] =  SW1_nothing_action,
	[BL1] =		SW1_nothing_action,
	[BL2] =		SW1_nothing_action,
	[PASSIVE] = SW1_nothing_action,
	[BH1] =		SW1_nothing_action,
	[BH2] =		SW1_nothing_action,
	[BH3] =		SW1_nothing_action
	
};


msg_t (*buttonClear_FncPtr[])(void)= {
	
	[BL3] =		SW2_action,
	[ACTIVE] =  SW2_nothing_action,
	[BL1] =		SW2_nothing_action,
	[BL2] =		SW2_nothing_action,
	[PASSIVE] = SW2_nothing_action,
	[BH1] =		SW2_nothing_action,
	[BH2] =		SW2_nothing_action,
	[BH3] =		SW2_nothing_action

};

msg_t (*buttonSetAlarm_FncPtr[])(void)= {
	
	[BL3] =		SW3_offAction,
	[ACTIVE] =  SW3_nothing_action,
	[BL1] =		SW3_nothing_action,
	[BL2] =		SW3_nothing_action,
	[PASSIVE] = SW3_nothing_action,
	[BH1] =		SW3_nothing_action,
	[BH2] =		SW3_nothing_action,
	[BH3] =		SW3_action

};


/*
 **************************************************************************************************
 *										Moore Table
 **************************************************************************************************
 */

transition_t STT1[NUM_TRANS] = {
    {PASSIVE,  HIGH,     BH1},
    {PASSIVE,  LOW,		 PASSIVE},
    {BH1,      HIGH,     BH2},
    {BH1,      LOW,		 PASSIVE},
    {BH2,      HIGH,     BH3},
    {BH2,      LOW,		 PASSIVE},
    {BH3,      HIGH,     ACTIVE},
    {BH3,      LOW,		 ACTIVE},
	{ACTIVE,   HIGH,	 ACTIVE},
	{ACTIVE,   LOW,      BL1},
	{BL1,      HIGH,     ACTIVE},
	{BL1,      LOW,      BL2},
	{BL2,      HIGH,     ACTIVE},
	{BL2,      LOW,	     BL3},
	{BL3,      HIGH,	 PASSIVE},
	{BL3,      LOW,	     PASSIVE}};

/*
 **************************************************************************************************
 *											Main
 **************************************************************************************************
 */

//typdef somestate

extern alarmONOFFState alarmState;

int inputTask(void)
{	
	qInc = fsm(qInc, STT1, buttonIncrement_FncPtr);
	qDec= fsm(qDec, STT1, buttonDecrement_FncPtr);
	qClear= fsm(qClear, STT1, buttonClear_FncPtr);
	//qSetAlarm = fsm(qSetAlarm, STT1, buttonSetAlarm_FncPtr); 
	if(alarmState == ONSTATE && (PIND & 0X40))
	{
		alarmState = OFFSTATE;
	}
	if(alarmState == OFFSTATE && (PIND & 0X040) == 0)
	{
		alarmState = ONSTATE;
	}
}

/*
 **************************************************************************************************
 *									Static Functions
 **************************************************************************************************
 */
static msg_t SW0_action(void)
{
	// Message sent to the clock. A minute increment has been pressed
	buttonInputVar.hrMin_Inc = MIN_INC;
	enqueueButton(buttonInputVar,&input_button_Queue);
	
	//=======================================
	
	if(PIND & 0x01)
	{
		return LOW;
	}
	else
	{
		return HIGH;
	}
}
static msg_t SW1_action(void)
{
	//Message enqueued that a minute inc was pressed
	buttonInputVar.hrMin_Inc = HR_INC;
	enqueueButton(buttonInputVar, &input_button_Queue);
	
	//=============================================
	
	if(PIND & 0x02)
	{
		return LOW;
	}
	else
	{
		return HIGH;
	}
}
static msg_t SW2_action(void)
{
	//Setting the mode based off of the previous mode
	if(buttonInputVar.modeSelect == NORMAL)
		buttonInputVar.modeSelect = ALARM_SET;
	else
		buttonInputVar.modeSelect = NORMAL;
	
	//Message enqueued that a minute inc was pressed
	enqueueButton(buttonInputVar, &input_button_Queue);
	
	//=============================================
	if(PIND & 0x04)
	{
		return LOW;
	}
	else
	{
		return HIGH;
	}
}

static msg_t SW3_action(){
	
	buttonInputVar.alarmState = ON;
	enqueueButton(buttonInputVar, &input_button_Queue);
	
	if (PIND & 0x40)
	{
		return LOW;
	}
	else
		return HIGH;
}

static msg_t SW3_offAction(){
	
	buttonInputVar.alarmState = OFF;
	enqueueButton(buttonInputVar, &input_button_Queue);
	
	if (PIND & 0x40)
	{
		return LOW;
	}
	else
	return HIGH;
}



static msg_t SW0_nothing_action(void)
{
	buttonInputVar.hrMin_Inc = NO_MESSAGE;
	if(PIND & 0x01)
	{
		return LOW;
	}
	else
	{
		return HIGH;
	}
}
static msg_t SW1_nothing_action(void)
{
	buttonInputVar.hrMin_Inc = NO_MESSAGE;
	if(PIND & 0x02)
	{
		return LOW;
	}
	else
	{
		return HIGH;
	}
}
static msg_t SW2_nothing_action(void)
{
	buttonInputVar.hrMin_Inc = NO_MESSAGE;
	if(PIND & 0x04)
	{
		return LOW;
	}
	else
	{
		return HIGH;
	}
}

static msg_t SW3_nothing_action(void)
{
	if(PIND & 0x40)
	{
		return LOW;
	}
	else
	{
		return HIGH;
	}
}

static state_codes_t lookup_transition(state_codes_t q0, msg_t m, transition_t STT[])
{
	for(int i = 0; i < NUM_TRANS; i++)
	{
		if(STT[i].src_state == q0 && STT[i].ret_code == m)
		return STT[i].dst_state;
	}
	return q0;
}
static state_codes_t fsm(state_codes_t q, transition_t STT[],msg_t (*moore_table[])(void))
{    
	msg_t (*selected_action)(void);
	msg_t m;
	
	selected_action = moore_table[q];
	m = selected_action();
	q = lookup_transition(q, m, STT);
	return q;
}