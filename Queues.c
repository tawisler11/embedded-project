/*
 * Queues.c
 *
 * Created: 10/31/2013 11:31:26 PM
 *  Author: rdhammond11
 */ 


	#include <avr/io.h>
	#include <avr/interrupt.h>
	#include <stdbool.h>
	#include "Queues.h"
		
		
	//////////////////////////////////////////////////////////////////////////
	///////////////// QUEUE FUNCTIONS FOR QUEUE CLOCKTIME TYPES //////////////
	//////////////////////////////////////////////////////////////////////////
	
	void queue_init(Queue_clocktime_types *q)
	{
		q->front = q->Buffer;
		q->back = q->Buffer;
	}
	
	bool isQueueEmpty(const Queue_clocktime_types *q){
		return (q->front == q->back);
	} 
	
	bool isQueueFull(const Queue_clocktime_types *q){
		if((q->front-q->back)==1) return true;
		else if ((q->back-q->front)==(QSIZE-1)) return true;
		else return false;
		
	}

	void enqueue(clock_time msg, Queue_clocktime_types *q){
		int8_t saved_SREG = SREG;
		cli();
		if(!(isQueueFull(q))){

			q->back->hr = msg.hr;
			q->back->minute = msg.minute;
			q->back->AM_or_PM = msg.AM_or_PM;
			q->back->noMessage = msg.noMessage;
			
			(q->back)++;
			if(q->back - q->Buffer >= QSIZE)
			{
				q->back = (q->Buffer);
			}
		}
		SREG = saved_SREG;
	}	
	
	clock_time dequeue(Queue_clocktime_types *q){
		int8_t saved_SREG = SREG;
		clock_time returnVal;
		cli();
		if(isQueueEmpty(q))
		{
			SREG = saved_SREG;
			return returnVal;
		}
		else
		{
			returnVal.hr = q->front->hr;
			returnVal.minute = q->front->minute;
			returnVal.AM_or_PM = q->front->AM_or_PM;
			returnVal.noMessage = q->front->noMessage;

			(q->front)++;
			if(q->front - q->Buffer >= QSIZE)
			{
				q->front = (q->Buffer);
			}
			SREG = saved_SREG;
			return returnVal;
		}
	}	



	//////////////////////////////////////////////////////////////////////////
	///////////////// QUEUE FUNCTIONS FOR INPUT BUTTON TYPES //////////////
	//////////////////////////////////////////////////////////////////////////

	void buttonQueue_init(Queue_buttonInput_types *q)
	{
		q->front = q->Buffer;
		q->back = q->Buffer;
	}
	
	bool isButtonQueueEmpty(const Queue_buttonInput_types *q){
		return (q->front == q->back);
	}
	
	bool isButtonQueueFull(const Queue_buttonInput_types *q){
		if((q->front-q->back)==1) return true;
		else if ((q->back-q->front)==(QSIZE-1)) return true;
		else return false;
		
	}
	
	void enqueueButton(buttonInput msg, Queue_buttonInput_types *q){
		int8_t saved_SREG = SREG;
		cli();
		if(!(isButtonQueueFull(q))){

			q->back->hrMin_Inc = msg.hrMin_Inc;
			q->back->modeSelect = msg.modeSelect;
			q->back->alarmState = msg.alarmState;
			
			(q->back)++;
			if(q->back - q->Buffer >= QSIZE)
			{
				q->back = (q->Buffer);
			}
		}
		SREG = saved_SREG;
	}
	
	buttonInput dequeueButton(Queue_buttonInput_types *q){
		int8_t saved_SREG = SREG;
		buttonInput returnVal;
		cli();
		if(isButtonQueueEmpty(q))
		{
			SREG = saved_SREG;
			return returnVal;
		}
		else
		{
			returnVal.hrMin_Inc = q->front->hrMin_Inc;
			returnVal.modeSelect = q->front->modeSelect;
			returnVal.alarmState = q->front->alarmState;
			(q->front)++;
			if(q->front - q->Buffer >= QSIZE)
			{
				q->front = (q->Buffer);
			}
			SREG = saved_SREG;
			return returnVal;
		}
	}
	
	//======================================================================================
	
	
	void tickQueue_init(Queue_tickInput_types *q)
	{
		q->front = q->Buffer;
		q->back = q->Buffer;
	}
	
	bool istickQueueEmpty(const Queue_tickInput_types *q){
		return (q->front == q->back);
	}
	
	bool istickQueueFull(const Queue_tickInput_types *q){
		if((q->front-q->back)==1) return true;
		else if ((q->back-q->front)==(QSIZE-1)) return true;
		else return false;
		
	}
	
	void enqueueTick(tickInput msg, Queue_tickInput_types *q){
		int8_t saved_SREG = SREG;
		cli();
		if(!(isQueueFull(q))){

			q->back->tick_inc = msg.tick_inc;
			
			(q->back)++;
			if(q->back - q->Buffer >= QSIZE)
			{
				q->back = (q->Buffer);
			}
		}
		SREG = saved_SREG;
	}
	
	tickInput dequeueTick(Queue_tickInput_types *q){
		int8_t saved_SREG = SREG;
		tickInput returnVal;
		cli();
		if(istickQueueFull(q))
		{
			SREG = saved_SREG;
			return returnVal;
		}
		else
		{
			returnVal.tick_inc = q->front->tick_inc;
			
			(q->front)++;
			if(q->front - q->Buffer >= QSIZE)
			{
				q->front = (q->Buffer);
			}
			SREG = saved_SREG;
			return returnVal;
		}
	}
	
	
	
	//////////////////////////////////////////////////////////////////////////
	///////////////// Queue Functions for alarm set enable/disable ///////////
	//////////////////////////////////////////////////////////////////////////
	
	void queue_Alarm_init(Queue_alarmInput_types *q)
	{
		q->front = q->Buffer;
		q->back = q->Buffer;
	}
	
	bool isAlarmQueueEmpty(const Queue_alarmInput_types *q){
		return (q->front == q->back);
	}
	
	bool isAlarmQueueFull(const Queue_alarmInput_types *q){
		if((q->front-q->back)==1) return true;
		else if ((q->back-q->front)==(QSIZE-1)) return true;
		else return false;
		
	}

	void enqueueAlarm(alarm_input msg, Queue_alarmInput_types *q){
		int8_t saved_SREG = SREG;
		cli();
		if(!(isQueueFull(q))){

			q->back->alarmState = msg.alarmState;
			
			(q->back)++;
			if(q->back - q->Buffer >= QSIZE)
			{
				q->back = (q->Buffer);
			}
		}
		SREG = saved_SREG;
	}
	
	alarm_input dequeueAlarm(Queue_alarmInput_types *q){
		int8_t saved_SREG = SREG;
		alarm_input returnVal;
		cli();
		if(isQueueEmpty(q))
		{
			SREG = saved_SREG;
			return returnVal;
		}
		else
		{
			returnVal.alarmState = q->front->alarmState;

			(q->front)++;
			if(q->front - q->Buffer >= QSIZE)
			{
				q->front = (q->Buffer);
			}
			SREG = saved_SREG;
			return returnVal;
		}
	}