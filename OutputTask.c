/*
 * OutputTask.c
 *
 * Created: 11/8/2013 3:36:06 PM
 *  Author: rdhammond11
 */ 


#include <avr/io.h>
#include <stdbool.h>
#include "Queues.h"
#define TRANSNUM 22

//=========================== Global Vars ===================== //
alarm_input alarm_EnDisable;

//=========================== External Functions ===================== //
alarm_input dequeueAlarm(Queue_alarmInput_types *q);
bool isAlarmQueueEmpty(const Queue_alarmInput_types *q);

//=========================== State Alphabet ===================== //
typedef enum {DC_OFF, DC_ON, DC_20, DC_30, DC_40, DC_50, DC_60, DC_80} duty_Cylec_states_t;

//=========================== Input Alphabet ===================== //
typedef enum {TIMESTART, TIME5, TIME15, TIME10, TIME20, TIME25, TIMEMAX, ALARMOFF, DONOTHING} time_msg_t;
	
typedef enum {SOUNDING, QUIET} alarm_state;
	
//===================== Transition Table Structure =============== //
typedef struct{duty_Cylec_states_t beginState; time_msg_t return_msg; duty_Cylec_states_t endingState;} dcTransitions_t;
	
//====================== Function Declerations ====================//
static time_msg_t off_action();
static time_msg_t on_action();
static time_msg_t on_20_action();
static time_msg_t on_30_action();
static time_msg_t on_40_action();
static time_msg_t on_50_action();
static time_msg_t on_60_action();
static time_msg_t on_80_action();
static duty_Cylec_states_t fsm1(duty_Cylec_states_t q1, dcTransitions_t inputTable[],time_msg_t (*moore_table[])(void));
static duty_Cylec_states_t lookup_transition(duty_Cylec_states_t inState, time_msg_t inMessage, dcTransitions_t inputTable[]);

static alarm_state alarm_state_sound = QUIET;
static duty_Cylec_states_t q1 = DC_OFF;	
static int8_t ticksFromClock = 0;

extern Queue_tickInput_types alarm_Tick_Storage;
extern Queue_tickInput_types alarm_state_Tick_Storage;
tickInput dequeueTick(Queue_tickInput_types *q);
bool istickQueueEmpty(const Queue_tickInput_types *q);
	
	
/*
 **************************************************************************************************
 *									Function Pointer Arrays
 **************************************************************************************************
 */

time_msg_t (*changingDC_FncPtr[])(void) = {  
	
	[DC_OFF] = off_action,
	[DC_ON] =  on_action,
	[DC_20] =  on_20_action, 
	[DC_30] =  on_30_action, 
	[DC_40] =  on_40_action,
	[DC_50] =  on_50_action,
	[DC_60] =  on_60_action,
	[DC_80] =  on_80_action,
	
};

/*
 **************************************************************************************************
 *										Moore Table
 **************************************************************************************************
 */

dcTransitions_t SATETABLE[TRANSNUM] = {
    {DC_OFF,   TIMESTART,     DC_ON},
    {DC_OFF,   DONOTHING,	  DC_OFF},
	{DC_ON,    TIMESTART,     DC_20},
	{DC_ON,    DONOTHING,     DC_ON},
	{DC_ON,    ALARMOFF,      DC_OFF},
    {DC_20,    TIME5,         DC_30},
    {DC_20,    DONOTHING,	  DC_20},
	{DC_20,    ALARMOFF,      DC_OFF},
    {DC_30,    TIME10,        DC_40},
    {DC_30,    DONOTHING,	  DC_30},
	{DC_30,    ALARMOFF,      DC_OFF},
    {DC_40,    TIME15,        DC_50},
    {DC_40,    DONOTHING,     DC_40},
	{DC_40,    ALARMOFF,      DC_OFF},
	{DC_50,    TIME20,	      DC_60},
	{DC_50,    DONOTHING,     DC_50},
	{DC_50,    ALARMOFF,      DC_OFF},
	{DC_60,    TIME25,        DC_80},
	{DC_60,    DONOTHING,     DC_60},
    {DC_60,    ALARMOFF,      DC_OFF},
	{DC_80,    DONOTHING,     DC_80},
	{DC_80,    ALARMOFF,	  DC_OFF}};

extern alarmONOFFState alarmState;

void outputTask()
{	
	if(!istickQueueEmpty(&alarm_state_Tick_Storage))
	{
		dequeueTick(&alarm_state_Tick_Storage);
		//if(alarm_EnDisable.alarmState == ON)
			alarm_state_sound = SOUNDING;
				
	}
	if(!istickQueueEmpty(&alarm_Tick_Storage))
	{
		dequeueTick(&alarm_Tick_Storage);
		ticksFromClock++;
	}
	
	
		q1 = fsm1(q1, SATETABLE, changingDC_FncPtr);		
		
	
}

time_msg_t off_action(){
	
	ticksFromClock = 0;
	TCCR0 = 0x0;
	TCCR1A = 0x0;
	TCCR1B = 0x0;
	OCR1B = 0;
	//PORTD |= 0x10;
	OCR0 = 0; //PWM for the light
	
	if(alarmState == OFFSTATE){
		alarm_state_sound = QUIET;
		return DONOTHING;
	}		
	if(alarm_state_sound == SOUNDING && alarmState == ONSTATE)
		return TIMESTART; 
}



time_msg_t on_action()
{
	TCCR0 = 0x6C; //Timer settings for the light
	OCR0 = 5; //PWM for the light
	
	if(alarmState == OFFSTATE)
		return ALARMOFF;
		
	if(ticksFromClock == 5)
	{
		ticksFromClock = 0;
		return TIMESTART;
	}	
	
	else
		return DONOTHING;
	
}

time_msg_t on_20_action()
{
	TCCR1A = 0xA3; // non-inverting mode
	TCCR1B = 0x1D; // Fast PWM, 8MHz clock, 1024 prescaler
	OCR1A = 7628; // 0-Top is 5 seconds
	OCR0 = 15;
	OCR1B = 7400;
	
	if(alarmState == OFFSTATE){
		alarm_state_sound = QUIET;
		return ALARMOFF;
	}		
	
	if(ticksFromClock == 5)
	{
		ticksFromClock = 0;
		return TIME5;
	}		
	
	else 
		return DONOTHING;
}

time_msg_t on_30_action()
{
	
	OCR0 = 20;
	OCR1B =  7300;
	
	if(alarmState == OFFSTATE){
		alarm_state_sound = QUIET;
		return ALARMOFF;
	}
	
	if(ticksFromClock == 5)
	{
		ticksFromClock = 0;
		return TIME10;
	}
	
	
	else
		return DONOTHING;	
}

time_msg_t on_40_action()
{
	OCR0 = 30;
	OCR1B =  7200;
	
	if(alarmState == OFFSTATE){
		alarm_state_sound = QUIET;
		return ALARMOFF;
	}
	
	if(ticksFromClock == 5)
	{
		ticksFromClock = 0;
		return TIME15;
	}
	
	else
		return DONOTHING;
}

time_msg_t on_50_action()
{
	OCR0 = 40;
	OCR1B =  7100;
	
	if(alarmState == OFFSTATE){
		alarm_state_sound = QUIET;
		return ALARMOFF;
	}
	
	if(ticksFromClock == 5)
	{
		ticksFromClock = 0;
		return TIME20;
	}
	
	else
		return DONOTHING;
}

time_msg_t on_60_action()
{
	OCR0 = 50;
	OCR1B =  7000;
	
	if(alarmState == OFFSTATE){
		alarm_state_sound = QUIET;
		return ALARMOFF;
	}
	
	if(ticksFromClock == 5)
	{
		ticksFromClock = 0;
		return TIME25;
	}
	
	
	else
		return DONOTHING;
}

time_msg_t on_80_action()
{
	OCR0 = 60;
	OCR1B = 6900;
	
	if(alarmState == OFFSTATE){
		alarm_state_sound = QUIET;
		return ALARMOFF;
	}
	
	else
		return DONOTHING;
}

static duty_Cylec_states_t lookup_transition(duty_Cylec_states_t inState, time_msg_t inMessage, dcTransitions_t inputTable[])
{
	for(int i = 0; i < TRANSNUM; i++)
	{
		if(inputTable[i].beginState == inState && inputTable[i].return_msg == inMessage)
		return inputTable[i].endingState;
	}
	return inState;
}
static duty_Cylec_states_t fsm1(duty_Cylec_states_t q1, dcTransitions_t inputTable[],time_msg_t (*moore_table[])(void))
{
	time_msg_t (*selected_action)(void);
	time_msg_t m;
	
	selected_action = moore_table[q1];
	m = selected_action();
	q1 = lookup_transition(q1, m, inputTable);
	return q1;
}






