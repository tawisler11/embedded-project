/*
 * Counter_for_display_V2.c
 *
 * Created: 10/29/2013 4:22:13 PM
 *  Author: rdhammond11
 * System Clock: 8 MHz
 * Crystal wired to PC6 & PC7
 * Display wired to PB0, PB1, and PORTD
 * Timer 2 and its compare is being used for a control variable tick
 * Timer 1 is being used for PWM
 */ 

/*
*********************************************************************************************************************************
 *														Includes
*********************************************************************************************************************************
 */
#include <avr/io.h>
#include <avr/interrupt.h>
#include "Queues.h"
#include "aux_globals.h"
#include "HD44780.h"
#include <stdbool.h>

/*
*********************************************************************************************************************************
 *													Macros and Definitions
*********************************************************************************************************************************
 */
alarmONOFFState alarmState;

/*
*********************************************************************************************************************************
 *													Global Variable Declaration
*********************************************************************************************************************************
 */

tickInput tickVar;

Queue_clocktime_types time_Storage;
Queue_buttonInput_types input_button_Queue;
Queue_tickInput_types tick_Storage;
Queue_tickInput_types alarm_Tick_Storage;
Queue_tickInput_types alarm_state_Tick_Storage;
Queue_clocktime_types alarm_Storage;


/*
*********************************************************************************************************************************
 *													Global function declarations
*********************************************************************************************************************************
 */

void queue_init(Queue_clocktime_types *q);
void tickQueue_init(Queue_tickInput_types *q);
void buttonQueue_init(Queue_buttonInput_types *q);
void queue_Alarm_init(Queue_alarmInput_types *q);


void clock_init(void);
void alarmTime_init(void);
void enqueueTick(tickInput msg, Queue_tickInput_types *q);

/*
*********************************************************************************************************************************
 *													Static function declarations
*********************************************************************************************************************************
 */
static void alarm_init(void);

/*
*********************************************************************************************************************************
 *														ISR Functions
*********************************************************************************************************************************
 */
ISR(TIMER2_COMP_vect)
{
	enqueueTick(tickVar, &tick_Storage);
	
	//PORTC ^= 0xFF;
}

int main(void){
/*
=============================
		DDR Setup
		  Begin
=============================
*/
	DDRA = 0xFF;
	DDRD = 0x30;
	DDRB = 0x0B;
	DDRC = 0x00;
/*
=============================
		DDR Setup
		   End
=============================
*/

/*
=============================
		Timer Used by
		Clock Task Settings
=============================
*/

	TCCR2 = 0x0E;// CTC MOde
	TIMSK = 0x80;// Interrupts enabled
	ASSR = 0x08; // external clock being used
	OCR2 = 255;  // 2 second periods
/*
=============================
		Timer2 Setup
			End
=============================
*/
	
/*
=============================
		Timer for PWM
			SetUp
=============================
*/

	TCCR1A = 0x0;
	TCCR1B = 0x0;

	
/*
=============================
		Timer for PWM
			End
=============================
*/

/*
=============================
		Initialization
			Begin
=============================
*/
	
	
	alarm_init();
	sei();
/*
=============================
		Initialization
			End
=============================
*/
	
	/*
	=============================
			Main Loop
	=============================
	*/
	while(1)
	{
			displayTask();
			inputTask();
			clockTask();
			outputTask();
	}
}

static void alarm_init(void)
{
	lcd_init();
	
	buttonQueue_init(&input_button_Queue);
	
	queue_init(&time_Storage);
	queue_init(&alarm_Storage);
	tickQueue_init(&tick_Storage);
	tickQueue_init(&alarm_state_Tick_Storage);
	
	alarmTime_init();
	clock_init();
	
	alarmState = OFFSTATE;
	
	tickVar.tick_inc = tick;
	
	//PORTD |= 0x10;
}
