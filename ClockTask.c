/*
 * ClockTask.c
 *
 * Created: 11/6/2013 7:22:37 PM
 *  Author: tawisler11
 */ 
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdbool.h>
#include "Queues.h"

extern Queue_tickInput_types tick_Storage;
extern Queue_clocktime_types time_Storage;
extern Queue_buttonInput_types input_button_Queue;
extern Queue_clocktime_types alarm_Storage;
extern Queue_tickInput_types alarm_Tick_Storage;
extern Queue_tickInput_types alarm_state_Tick_Storage;


bool istickQueueEmpty(const Queue_tickInput_types *q);
bool isButtonQueueEmpty(const Queue_buttonInput_types *q);
void setTime();
void setAlarm();

tickInput dequeueTick(Queue_tickInput_types *q);
buttonInput dequeueButton(Queue_buttonInput_types *q);

void enqueue(clock_time msg, Queue_clocktime_types *q);
void enqueueTick(tickInput msg, Queue_tickInput_types *q);
void enqueueAlarm(alarm_input msg, Queue_alarmInput_types *q);

alarm_input alarm_enable_var;
tickInput tickVar_clock;
clock_time clockVar;
clock_time alarmVar;
buttonInput buttonVar;
int16_t pulse;
int16_t alarmPulseVar;
int64_t test;
tickInput tickVar;

void clock_init(void)
{
	clockVar.hr = 12;
	clockVar.minute = 00;
	clockVar.AM_or_PM = AM;
	clockVar.noMessage = 0;
	enqueue(clockVar, &time_Storage);
	
	tickVar.tick_inc = tick;
}

void alarmTime_init(void)
{
	alarmVar.hr = 12;
	alarmVar.minute = 00;
	alarmVar.AM_or_PM = AM;
	alarmVar.noMessage = 0;
	enqueue(alarmVar, &alarm_Storage);
}

void clockTask(void)
{
	
	if(!isButtonQueueEmpty(&input_button_Queue))
	{
		buttonVar = dequeueButton(&input_button_Queue);
		if(buttonVar.modeSelect != ALARM_SET){
			setTime();
			
		} // End of checking for alarm set
		else if (buttonVar.modeSelect == ALARM_SET)
		{	
			setAlarm();
		}
		
		/*
		if(buttonVar.alarmState == OFF)
		{
			alarm_enable_var.alarmState = OFF;
			enqueueAlarm(alarm_enable_var, &alarm_state_Tick_Storage);
		}
		else
			alarm_enable_var.alarmState = ON;
		*/
	} 	
	
		
	
	if(!istickQueueEmpty(&tick_Storage))
	{
		tickVar_clock = dequeueTick(&tick_Storage);
		pulse++;
	}

	if(pulse == 30) //30 for real //10 for testing
	{
		
		enqueueTick(tickVar,&alarm_Tick_Storage); //Timing for alarm. Sent to OutputTask
		
		pulse = 0;
		clockVar.minute++;
			
		if(clockVar.minute == 60)
		{
			clockVar.minute = 0;
			clockVar.hr++;
				
			if(clockVar.hr == 12)
			{
				if(clockVar.AM_or_PM == AM)
				{
					clockVar.AM_or_PM = PM;
				}
				else
				{
					clockVar.AM_or_PM = AM;
				}
				if(buttonVar.modeSelect != ALARM_SET) enqueue(clockVar,&time_Storage);
			}
			else if(clockVar.hr == 13)
			{
				clockVar.hr = 1;
				if(buttonVar.modeSelect != ALARM_SET) enqueue(clockVar,&time_Storage);
			}
			else
			{
				if(buttonVar.modeSelect != ALARM_SET) enqueue(clockVar,&time_Storage);
			}
		}
		else
		{
			if(buttonVar.modeSelect != ALARM_SET) enqueue(clockVar,&time_Storage);
		}
		if(alarmVar.hr == clockVar.hr && alarmVar.minute == clockVar.minute && alarmVar.AM_or_PM == clockVar.AM_or_PM)
		{
			//alarm_enable_var.alarmState = ON;
			enqueueTick(tickVar, &alarm_state_Tick_Storage);
			//enqueueTick(tickVar,&alarm_Tick_Storage);
		}			
	}
}



void setTime(){
	
	if(buttonVar.hrMin_Inc == HR_INC)
	{
		clockVar.hr++;
		pulse = 0;
		
		if(clockVar.hr == 12)
		{
			if(clockVar.AM_or_PM == AM)
			{
				clockVar.AM_or_PM = PM;
			}
			else
			{
				clockVar.AM_or_PM = AM;
			}
		}
		else if(clockVar.hr == 13)
		{
			clockVar.hr = 1;
		}
	}
	if(buttonVar.hrMin_Inc == MIN_INC)
	{
		clockVar.minute++;
		pulse = 0;
		if(clockVar.minute == 60)
		{
			clockVar.minute = 0;
		}
	}
	enqueue(clockVar,&time_Storage);
	
}


void setAlarm(){
	
	if(buttonVar.hrMin_Inc == HR_INC)
	{
		alarmVar.hr++;
		
		if(alarmVar.hr == 12)
		{
			if(alarmVar.AM_or_PM == AM)
			{
				alarmVar.AM_or_PM = PM;
			}
			else
			{
				alarmVar.AM_or_PM = AM;
			}
		}
		else if(alarmVar.hr == 13)
		{
			alarmVar.hr = 1;
		}
	}
	if(buttonVar.hrMin_Inc == MIN_INC)
	{
		alarmVar.minute++;
		if(alarmVar.minute == 60)
		{
			alarmVar.minute = 0;
		}
	}
	enqueue(alarmVar,&alarm_Storage);
	
}